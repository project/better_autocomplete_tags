DESCRIPTION
===========
This module provides better styles for autocomplete tags widgets
using Bootstrap Tags Input and Twitter Typeahead plugins.

SUPPORTED FIELD TYPES
=====================
Term reference - Autocomplete (tagging)
Entity reference - Autocomplete (tags style)

REQUIREMENTS
============
jQuery version >= 1.7.0
