/**
 * @file
 * Defines initialization functions.
 */

(function ($) {

  "use strict";

  /**
   * Add bootstrap-tagsinput UI.
   */
  Drupal.behaviors.betterAutocompleteTags = {
    attach: function (context, settings) {
      $.each(Drupal.settings.better_autocomplete_tags, function (field_name, field_data) {
        // Create typeahead object.
        var typeahead = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          remote: {
            url: field_data.autocomplete_path,
            prepare: function (query, settings) {
              settings.url = settings.url + '/' + query;
              return settings;
            },
            filter: function (list) {
              return $.map(list, function (data, tag) {
                return {val: tag, name: tag.replace(/(.+)\s\(\d+\)/, '$1')};
              });
            }
          }
        });

        // Initialize typeahead
        typeahead.initialize();

        // Add bootstrap-tagsinput UI to field
        $('input.' + field_name + '-bootstrap-tagsinput-input').tagsinput({
          typeaheadjs: {
            name: 'tags',
            displayKey: 'name',
            valueKey: 'val',
            source: typeahead.ttAdapter()
          },
          trimValue: true,
          itemText: function (item) {
            return item.replace(/(.+)\s\(\d+\)/, '$1');
          }
        });
      });

      // HACK: overrule hardcoded display inline-block of typeahead.js
      $(".twitter-typeahead").css('display', 'inline');
    }
  };

})(jQuery);
